var classmain__dog_1_1Dog__Leg__Controller =
[
    [ "__init__", "classmain__dog_1_1Dog__Leg__Controller.html#a7fbbfb32ab5be552453e3a69dc5b73a7", null ],
    [ "balance_callback", "classmain__dog_1_1Dog__Leg__Controller.html#a5952f1f2529877bd54de24546a4c676a", null ],
    [ "pathTrack", "classmain__dog_1_1Dog__Leg__Controller.html#a02c72c56f447c6f58991884523d266e1", null ],
    [ "run", "classmain__dog_1_1Dog__Leg__Controller.html#ad40025ceba81b4ea1f21cbe2167b27c9", null ],
    [ "transitionTo", "classmain__dog_1_1Dog__Leg__Controller.html#a75b045cc14dea18aa6d45dc3dc6f82ad", null ],
    [ "balance", "classmain__dog_1_1Dog__Leg__Controller.html#a45d350e26c86350bd5df66bfa685b05d", null ],
    [ "balanced", "classmain__dog_1_1Dog__Leg__Controller.html#a273fad92371bf71efde598ba9890e2ea", null ],
    [ "curr_time", "classmain__dog_1_1Dog__Leg__Controller.html#aa8bd1b825d86de385946ed6bf317b422", null ],
    [ "current_time", "classmain__dog_1_1Dog__Leg__Controller.html#a08d9258aab5829976bebe177e94ba174", null ],
    [ "enc_1", "classmain__dog_1_1Dog__Leg__Controller.html#aea2bd39296e701091ac55c3eba1c1647", null ],
    [ "enc_2", "classmain__dog_1_1Dog__Leg__Controller.html#a9f20e0daf7a283383fb47df7fc2fce1a", null ],
    [ "error_th1", "classmain__dog_1_1Dog__Leg__Controller.html#a8cc54945e837565c646e89941bdee9ce", null ],
    [ "error_th2", "classmain__dog_1_1Dog__Leg__Controller.html#a791412fe78d08e179d04720ca956edee", null ],
    [ "error_time_delta", "classmain__dog_1_1Dog__Leg__Controller.html#a9811233e151da9628110949ea068c384", null ],
    [ "i", "classmain__dog_1_1Dog__Leg__Controller.html#a7b7ca37962d2f77d4388859646815410", null ],
    [ "last_time", "classmain__dog_1_1Dog__Leg__Controller.html#ac49db8a6718df1bfb41398b5432c4b34", null ],
    [ "motor_1", "classmain__dog_1_1Dog__Leg__Controller.html#a9f81c85424873b0926cc276007efd4bd", null ],
    [ "motor_2", "classmain__dog_1_1Dog__Leg__Controller.html#a6f4d7a72be84055cc3e46323f26fe025", null ],
    [ "next_time", "classmain__dog_1_1Dog__Leg__Controller.html#ac27c870c761eee70942737a5c36a1bf7", null ],
    [ "period", "classmain__dog_1_1Dog__Leg__Controller.html#a1eea4d52650c6d6e033047f544b25a75", null ],
    [ "r_1", "classmain__dog_1_1Dog__Leg__Controller.html#aa709fe26cccbe6102ca046e4eedce49c", null ],
    [ "r_2", "classmain__dog_1_1Dog__Leg__Controller.html#ad5f3e5150062bf5669e700d38579f4b7", null ],
    [ "start_time", "classmain__dog_1_1Dog__Leg__Controller.html#a456b4a0bb1eaa1e26841985a67c14258", null ],
    [ "state", "classmain__dog_1_1Dog__Leg__Controller.html#af297d11a64d2bcecf255a7473f24c0c3", null ],
    [ "th1_coeff", "classmain__dog_1_1Dog__Leg__Controller.html#af14cb59cf1000d0b58935fa7db51337d", null ],
    [ "th2_coeff", "classmain__dog_1_1Dog__Leg__Controller.html#a6d11aa9409afe393652d0fbc93dcd275", null ],
    [ "theta_1", "classmain__dog_1_1Dog__Leg__Controller.html#a6f9f92b6a26412ac8c39c55321d1e336", null ],
    [ "theta_1_next", "classmain__dog_1_1Dog__Leg__Controller.html#a599110e0cf9fc1440577a0e4439fd522", null ],
    [ "theta_2", "classmain__dog_1_1Dog__Leg__Controller.html#ad60fc36270d376f14c49ca5b757ff4cc", null ],
    [ "theta_2_next", "classmain__dog_1_1Dog__Leg__Controller.html#a85e5fada977090eb8d88b50b7afdbe74", null ],
    [ "thetad_1", "classmain__dog_1_1Dog__Leg__Controller.html#aa907788e00d9af835d7a98886cfd6cdd", null ],
    [ "thetad_2", "classmain__dog_1_1Dog__Leg__Controller.html#af80262932e52290b4296452e95fbaa37", null ],
    [ "time", "classmain__dog_1_1Dog__Leg__Controller.html#ad758d5e0b85010e2b8e05243823b1cf8", null ],
    [ "total_time", "classmain__dog_1_1Dog__Leg__Controller.html#a050d96c4c07f4fddc8a953e9e052188a", null ],
    [ "w", "classmain__dog_1_1Dog__Leg__Controller.html#a0e0f0b292c4c712929896c0980ba2a29", null ],
    [ "x", "classmain__dog_1_1Dog__Leg__Controller.html#a418ebbfd86fb5c8787c48015b4ba5f40", null ],
    [ "x_c", "classmain__dog_1_1Dog__Leg__Controller.html#a5d881bfe3fa751e0d9418851d53e9749", null ],
    [ "x_next", "classmain__dog_1_1Dog__Leg__Controller.html#abad5839eb1428c5ba4a0d4e50df4a46d", null ],
    [ "y", "classmain__dog_1_1Dog__Leg__Controller.html#a96583668a5eaa02a647a600397a3a3d5", null ],
    [ "y_c", "classmain__dog_1_1Dog__Leg__Controller.html#a1a2e5c51ff829a7c82a30f1e55773a6e", null ],
    [ "y_next", "classmain__dog_1_1Dog__Leg__Controller.html#a717d597fc5207ed2c6219376d6027c48", null ]
];