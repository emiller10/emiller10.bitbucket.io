var annotated_dup =
[
    [ "MATLAB Code", "kinematic_model.html#subsec_matlab", null ],
    [ "Source Code", "kinematic_model.html#subsec_source", null ],
    [ "Documentation", "motor_encoder_driver.html#subsec_doc", null ],
    [ "Conclusions", "control_system.html#conc_controls", null ],
    [ "Encoder", null, [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "fault", null, [
      [ "nFAULT", "classfault_1_1nFAULT.html", "classfault_1_1nFAULT" ]
    ] ],
    [ "main", null, [
      [ "Dog_Leg_Controller", "classmain_1_1Dog__Leg__Controller.html", "classmain_1_1Dog__Leg__Controller" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "py", "classDog__Leg__Controller_1_1py.html", null ]
];